json.array!(@patients) do |patient|
  json.extract! patient, :id, :patient_name, :address_name, :insurance_id
  json.url patient_url(patient, format: :json)
end
