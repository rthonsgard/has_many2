# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Insurance.create(name: 'United Healthcare', street_address:'5 Corp St')
Insurance.create(name: 'Cigna', street_address: '44 Mega St')
Insurance.create(name: 'Blue Cross', street_address: '14 Main St')
Insurance.create(name: 'Assurant', street_address: '342 Second St')
Insurance.create(name: 'Humana', street_address: '4523 Broadway St')

Patient.create(patient_name: 'Russell Thonsgard', address_name: '123 Anywhere Ct', insurance_id: 1)
Patient.create(patient_name: 'Jane Doe', address_name: '789 Nowhere Ave', insurance_id: 2)
Patient.create(patient_name: 'John Smith', address_name: '155 Main St', insurance_id: 3)
Patient.create(patient_name: 'Dan Mills', address_name: '2255 Somewhere', insurance_id: 1)
Patient.create(patient_name: 'Holly Elliott', address_name: '4343 First St', insurance_id: 3)

Specialist.create(name: 'Dr. Hawkeye Pierce', specialty: 'ENT')
Specialist.create(name: 'Dr. Trapper John', specialty: 'Family Medicine')
Specialist.create(name: 'Dr. BJ Hunnicutt', specialty: 'Internal Medicine')
Specialist.create(name: 'Dr. Sherman Potter', specialty: 'Orthopaedic Surgery')
Specialist.create(name: 'Dr. Frank Burns', specialty: 'Dermatology')

Appointment.create(specialist_id: 1, patient_id: 1, complaint: "Cold", appointment_date: DateTime.httpdate('Wed, 08 Oct 2014 09:00:00 GMT'), fee: 25.00)
Appointment.create(specialist_id: 1, patient_id: 1, complaint: "Flu", appointment_date: DateTime.httpdate('Wed, 17 Sep 2014 09:00:00 GMT'), fee: 25.00)
Appointment.create(specialist_id: 3, patient_id: 1, complaint: "Abdominal Pain", appointment_date: DateTime.httpdate('Mon, 02 Jun 2014 11:00:00 GMT'), fee: 25.00)

Appointment.create(specialist_id: 4, patient_id: 2, complaint: "Back Pain", appointment_date: DateTime.httpdate('Wed, 08 Oct 2014 10:00:00 GMT'), fee: 25.00)
Appointment.create(specialist_id: 1, patient_id: 2, complaint: "Infection", appointment_date: DateTime.httpdate('Mon, 20 Oct 2014 09:00:00 GMT'), fee: 25.00)
Appointment.create(specialist_id: 1, patient_id: 2, complaint: "Flu", appointment_date: DateTime.httpdate('Wed, 17 Sep 2014 12:00:00 GMT'), fee: 25.00)

Appointment.create(specialist_id: 5, patient_id: 3, complaint: "Rash", appointment_date: DateTime.httpdate('Wed, 29 Oct 2014 11:00:00 GMT'), fee: 25.00)
Appointment.create(specialist_id: 2, patient_id: 3, complaint: "Broken Arm", appointment_date: DateTime.httpdate('Wed, 08 Oct 2014 09:00:00 GMT'), fee: 25.00)
Appointment.create(specialist_id: 1, patient_id: 3, complaint: "Flu", appointment_date: DateTime.httpdate('Tue, 04 Mar 2014 09:00:00 GMT'), fee: 25.00)

Appointment.create(specialist_id: 5, patient_id: 4, complaint: "Rash", appointment_date: DateTime.httpdate('Wed, 29 Oct 2014 11:00:00 GMT'), fee: 25.00)
Appointment.create(specialist_id: 1, patient_id: 4, complaint: "Cold", appointment_date: DateTime.httpdate('Thu, 09 Oct 2014 09:00:00 GMT'), fee: 25.00)
Appointment.create(specialist_id: 1, patient_id: 4, complaint: "Flu", appointment_date: DateTime.httpdate('Mon, 20 Oct 2014 09:00:00 GMT'), fee: 25.00)

Appointment.create(specialist_id: 5, patient_id: 5, complaint: "Rash", appointment_date: DateTime.httpdate('Wed, 29 Oct 2014 11:00:00 GMT'), fee: 25.00)
Appointment.create(specialist_id: 1, patient_id: 5, complaint: "Cold", appointment_date: DateTime.httpdate('Tue, 07 Oct 2014 09:00:00 GMT'), fee: 25.00)
Appointment.create(specialist_id: 3, patient_id: 5, complaint: "Flu", appointment_date: DateTime.httpdate('Mon, 17 Nov 2014 09:00:00 GMT'), fee: 25.00)