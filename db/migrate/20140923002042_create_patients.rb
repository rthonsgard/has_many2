class CreatePatients < ActiveRecord::Migration
  def change
    create_table :patients do |t|
      t.string :patient_name
      t.string :address_name
      t.integer :insurance_id

      t.timestamps
    end
  end
end
