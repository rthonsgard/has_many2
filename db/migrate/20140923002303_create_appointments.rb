class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      t.integer :specialist_id
      t.integer :patient_id
      t.string :complaint
      t.decimal :fee, :scale => 2
      t.datetime :appointment_date

      t.timestamps
    end
  end
end
