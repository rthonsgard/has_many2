# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
HasMany2::Application.config.secret_key_base = '2e34aafa048e565bab9dc27188aaf85f6398a1ffb98043b37cb7bf3b2141d161f75700da45e15b6308a916a5a7d4c255b0186adad1d7daf9a578c6717287c4aa'
